import java.util.LinkedList;

public class DoubleStack {

   private LinkedList<Double> array;

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   DoubleStack() {
      this.array = new LinkedList<>();
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();

      clone.array.addAll(array);

      return clone;
   }

   public boolean stEmpty() {
      return array.isEmpty();
   }

   public void push (double a) {
      array.push(a);
   }

   public double pop() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack underflow: " + this);
      }

      return array.pop();
   }

   public void op (String s) {
      switch (s) {
         case "+":
            opPlus();
            break;
         case "-":
            opMinus();
            break;
         case "*":
            opMultiply();
            break;
         case "/":
            opDivide();
            break;
         case "ROT":
            opRot();
            break;
         case "DUP":
            opDup();
            break;
         case "SWAP":
            opSwap();
            break;

         default:
            throw new IllegalArgumentException("Invalid operation: " + s);
      }
   }

   private void opPlus() {
      double buf1, buf2;

      try {
         buf1 = pop();
         buf2 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation +");
      }

      push(buf2 + buf1);
   }

   private void opMinus() {
      double buf1, buf2;

      try {
         buf1 = pop();
         buf2 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation -");
      }

      push(buf2 - buf1);
   }

   private void opMultiply() {
      double buf1, buf2;

      try {
         buf1 = pop();
         buf2 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation *");
      }

      push(buf2 * buf1);
   }

   private void opDivide() {
      double buf1, buf2;

      try {
         buf1 = pop();
         buf2 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation /");
      }

      push(buf2 / buf1);
   }

   public void opRot() {
      double buf1, buf2, buf3;

      try {
         buf1 = pop();
         buf2 = pop();
         buf3 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation ROT");
      }

      push(buf2);
      push(buf1);
      push(buf3);
   }

   public void opDup() {
      double buf1;

      try {
         buf1 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation DUP");
      }

      push(buf1);
      push(buf1);
   }

   public void opSwap() {
      double buf1, buf2;

      try {
         buf1 = pop();
         buf2 = pop();
      } catch (IndexOutOfBoundsException e) {
         throw new RuntimeException("Not enough numbers in stack to perform operation SWAP");
      }

      push(buf1);
      push(buf2);

   }
  
   public double tos() {
      if (stEmpty()) {
         throw new IndexOutOfBoundsException("Stack underflow: " + this);
      }

      return array.getFirst();
   }

   @Override
   public boolean equals (Object o) {
      if (!(o instanceof DoubleStack)) {
         return false;
      }

      DoubleStack doubleStack = (DoubleStack) o;

      if (doubleStack.array.size() != array.size()) {
         return false;
      }

      for (int i = 0; i <= doubleStack.array.size() - 1; i++) {
         if (!doubleStack.array.get(i).equals(array.get(i))) {
            return false;
         }
      }

      return true;
   }

   @Override
   public String toString() {
      if (stEmpty()) {
         return "Empty";
      }

      StringBuilder sb = new StringBuilder();

      for (int i = array.size() - 1; 0 <= i; i--) {
         sb.append(array.get(i));

         if (i - 1 != -1) {
            sb.append(" ");
         }
      }

      return sb.toString();
   }

   public static double interpret (String pol) {
      if (pol.isEmpty()) {
         throw new RuntimeException("Empty expression");
      }

      DoubleStack stack = new DoubleStack();

      String[] parts = pol.split("\\s+");

      for (String part : parts) {
         if (part.isEmpty()) {
            continue;
         }

         try {
            stack.push(Double.parseDouble(part));
         } catch (NumberFormatException error) {
            try {
               stack.op(part);
            } catch (IndexOutOfBoundsException e) {
               throw new RuntimeException("Cannot perform " + part + " in expression " + pol);
            } catch (IllegalArgumentException e) {
               throw new RuntimeException("Illegal symbol " + part + " in expression " + pol);
            }
         }
      }

      if (stack.array.size() > 1) {
         throw new RuntimeException("Too many numbers in expression " + pol);
      }

      return stack.tos();
   }
}

